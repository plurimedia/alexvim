let g:fuf_modesDisable = []
map <f2> :FufFile<cr>
map <f3> :FufMruFile<cr>
map <f4> :FufMruCmd<cr>
map <f5> :NERDTreeToggle<cr>
map <c-p> :set invpaste<cr>
map à :tabp<cr>
map ù :tabn<cr>
set ignorecase
set smartindent
set t_Co=256
set nrformats=
set backspace=indent,eol,start
set invnumber
set invrelativenumber

"Status bar
set ls=2
set statusline=%t[%{strlen(&fenc)?&fenc:'none'},%{&ff}]%h%m%r%y%=%c,%l/%L\ %P

"Learn VIM! Arrows only to move between split windows
" noremap <up> <C-w><up>
" noremap <down> <C-w><down>
" noremap <left> <C-w><left>
" noremap <right> <C-w><right>

" VIM Fuzzy search
set path+=**
set wildmenu
set wildignore=*/node_modules/*,*/bower_components/*,*/docker/*

let mapleader = "\<space>"
nnoremap <leader>w :w<cr>
nnoremap <leader>a vat
nnoremap <leader>i vit
nmap <leader>c Vgc
nnoremap <leader>e gf
"nnoremap <leader>f :find *
nnoremap <leader>l :set invnumber<cr>:set invrelativenumber<cr>

" Edit my ~/.vimrc
nnoremap <leader>rc :vsplit $MYVIMRC<cr>bn

" Remove spaces at end of line
nnoremap <leader>d<space> :let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar><CR>

" goto shell
nnoremap <c-d> :sh<cr>

" HTML tags indenter: Leader q
function! Indenter()
  let curpos = col('.')
  normal! j^
  let curpos2 = col('.')
  if curpos > curpos2
     execute "normal! i" . repeat(' ', curpos - curpos2)
     normal! l
  endif
  if curpos < curpos2
     execute "normal! " . (curpos2 - curpos) . "X"
  endif
endfunction

function! Indenter2()
   normal! ^f"B
   let cursor_pos = getpos('.')
   let first_line = line('.')
   let cerca = 1
   while cerca == 1
     let curpos = col('.')
     normal! 3f"
     let curpos2 = col('.')
     if curpos == curpos2
        let cerca = 0
        break
     else
        normal! ,li^
     endif
   endwhile

   let last_line = line('.')
   call setpos('.', cursor_pos)
   while first_line < last_line
      call Indenter()
      let first_line = first_line + 1
   endwhile
endfunction
nnoremap <leader>q :call Indenter2()<cr>

" Work with buffers
" set hidden
" let g:LustyJugglerDefaultMappings = 0
" nnoremap <leader>b :LustyJuggler<CR>
nnoremap <silent> <c-Right> :bn<CR>
nnoremap <silent> <c-Left> :bp<CR>

" Fix mouse reporting in large terminal windows
if has('mouse_sgr')
   set ttymouse=sgr
endif

"Uppercase
vnoremap <c-u> Ugv
nnoremap <c-u> vawUw
inoremap <c-u> <esc>mxvawU`xli

"Lowercase
vnoremap <c-l> ugv
nnoremap <c-l> vawuw
inoremap <c-l> <esc>mxvawu`xli

" Templates
"Ask for a class and create an empty div tag
nnoremap <expr> <leader>td "i<div class=\"" . input("Class: ") . "\"></div>\<esc>?<<cr>i<cr><esc>O"

"Paste from clipboard
"map <leader>p :set paste<cr>o<esc>"*]p:set nopaste<cr>

"Abbreviations
iabbrev ffun function()<cr>{<cr>}<esc>O<backspace>

" Indent management
function! SetTabs(width)
  execute "set softtabstop=" . a:width
  execute "set tabstop=" . a:width
  execute "set shiftwidth=" . a:width
endfunction
set expandtab
call SetTabs(2)
nnoremap <leader>2 :call SetTabs(2)<cr>
nnoremap <leader>3 :call SetTabs(3)<cr>
nnoremap <leader>4 :call SetTabs(4)<cr>

highlight LineNr ctermfg=grey ctermbg=white
"syntax on
"set background=dark
"colorscheme distinguished

let g:solarized_termcolors=256
syntax enable
set background=light
colorscheme solarized

" Easymotion n-character
" map  / <Plug>(easymotion-sn)
" omap / <Plug>(easymotion-tn)

" Escape special characters in a string for exact matching.
" This is useful to copying strings from the file to the search tool
" Based on this - http://peterodding.com/code/vim/profile/autoload/xolox/escape.vim
function! EscapeString(string)
  let string=a:string
  " Escape regex characters
  let string = escape(string, '^$.*\/~[]')
  " Escape the line endings
  let string = substitute(string, '\n', '\\n', 'g')
  return string
endfunction

" Get the current visual block for search and replaces
" This function passed the visual block through a string escape function
" Based on this - http://stackoverflow.com/questions/676600/vim-replace-selected-text/677918#677918
function! GetVisual() range
  " Save the current register and clipboard
  let reg_save = getreg('"')
  let regtype_save = getregtype('"')
  let cb_save = &clipboard
  set clipboard&

  " Put the current visual selection in the " register
  normal! ""gvy
  let selection = getreg('"')

  " Put the saved registers and clipboards back
  call setreg('"', reg_save, regtype_save)
  let &clipboard = cb_save

  "Escape any special characters in the selection
  let escaped_selection = EscapeString(selection)

  return escaped_selection
endfunction

" Start the find and replace command across the entire file
vnoremap <c-r> <esc>:,$s/<c-r>=GetVisual()<cr>//gc<left><left><left>

" Start the find file command for the current word
set wildcharm=<tab>
nnoremap <c-f> :find *<c-r><c-w><tab>

" Folding
"map <f9> zfat<cr>
"nnoremap <silent> <space> @=(foldlevel('.')?'za':"\<space>")<cr>
"vnoremap <space> zf

" Note: Skip initialization for vim-tiny or vim-small.
if !1 | finish | endif

if has('vim_starting')
   set nocompatible               " Be iMproved

   " Required:
   set runtimepath+=~/.vim/bundle/neobundle.vim/
endif

" Required:
call neobundle#begin(expand('~/.vim/bundle/'))

" Let NeoBundle manage NeoBundle
" Required:
NeoBundleFetch 'Shougo/neobundle.vim'

" My Bundles here:
" Refer to |:NeoBundle-examples|.
" Note: You don't set neobundle setting in .gvimrc!
NeoBundle 'posva/vim-vue'
NeoBundle 'justinmk/vim-sneak'
NeoBundle 'tpope/vim-commentary'
NeoBundle 'leafgarland/typescript-vim'
" NeoBundle 'vim-scripts/LustyJuggler'
NeoBundle 'tpope/vim-fugitive'
" NeoBundle 'vim-syntastic/syntastic'
" NeoBundle 'w0rp/ale'
" NeoBundle 'Shougo/unite'
" NeoBundle 'easymotion/vim-easymotion'

NeoBundle 'junegunn/fzf', { 'dir': '/usr/local/opt/fzf', 'do': 'yes \| ./install' }     
NeoBundle 'junegunn/fzf.vim'

call neobundle#end()

" Required:
filetype plugin indent on

" let g:ale_sign_column_always = 1
" let g:ale_linters = {
" \   'javascript': ['eslint'],
" \}

" If there are uninstalled bundles found on startup,
" this will conveniently prompt you to install them.
NeoBundleCheck
NeoBundleLazy 'jelera/vim-javascript-syntax', {'autoload':{'filetypes':['javascript']}}
"NeoBundle 'wincent/terminus'
"let g:TerminusMouse=0

hi link htmlLink none
autocmd FileType vue syntax sync fromstart

" Syntastic setup
" set statusline+=%#warningmsg#
" set statusline+=%{SyntasticStatuslineFlag()}
" set statusline+=%*
" let g:syntastic_always_populate_loc_list = 1
" let g:syntastic_auto_loc_list = 1
" let g:syntastic_check_on_open = 1
" let g:syntastic_check_on_wq = 0
" let g:syntastic_javascript_checkers = ['eslint']

command! MakeTags !ctags -R *

" fuzzy finder depends on:
" 1. brew install fzf
" 2. brew install ripgrep
" 3. in ~/.bashrc:
"  if type rg &> /dev/null; then
"    export FZF_DEFAULT_COMMAND='rg --files'
"    export FZF_DEFAULT_OPTS='-m --height 50% --border'
"  fi

" fuzzy finder commands
"search project files
"nnoremap <leader>p :FZF<cr>
"nnoremap \ :FZF<cr>
"nnoremap <silent> <leader>f :Rg<cr>
nmap <Leader>F :GFiles<CR>
nmap <Leader>f :Files<CR>
nmap <Leader>b :Buffers<CR>
nmap <Leader>h :History<CR>
"nmap <Leader>t :BTags<CR>
"nmap <Leader>T :Tags<CR>
nmap <Leader>r :BLines<CR>
nmap <Leader>R :Lines<CR>
nmap \ :Rg<Space>

" Tell Vim to use ripgrep instead of grep
set grepprg=rg\ --vimgrep\ --smart-case\ --follow
